const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.register = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((success, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	});
}

// User Login
module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	});
}

module.exports.order = async (data) => {
    if (data.isAdmin) {
    	return false
    } else {
    	let isUserUpdated = await User.findById(data.userId).then( user => {
    	user.orders.push({productId: data.productId});
    	return user.save().then( (user, error) => {
    		if(error) {
    			return false
    		} else {
    			return true
    		}
    	});
    });
    let isProductUpdated = await Product.findById(data.productId).then( product => {
    	product.orders.push({userId: data.userId})
    	return product.save().then((product, error) => {
    		if(error) {
    			return false
    		} else {
    			return true
    		}
    	})
    })
    if(isUserUpdated && isProductUpdated) {
    	return true
    } else {
    	return false
    }
    }
};

module.exports.details = (reqBody, reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		if(result != null) {
			result.password = '';
			return result;
		} else {
			return false;
		}
	})
}

module.exports.checkOut = async (data, productData) =>
	{
		if (data.isAdmin === false)
		{	
			
			let userDetails = await User.findById(data.userId).then(result => {return result}) 
				//console.log (userDetails); 

			let updateUser = await User.findById(data.userId).then(user =>
			{
				user.orders.push(
				{
					products: productData.products,
					totalAmount: productData.totalAmount
				})
				//console.log (productData)
				return user.save().then((user, error) =>
				{
					if (error)
						{	return false}
					else
						{	return	true}
				})
			});

			if (updateUser)
				{	/*userDetails = await User.findById(data.userId).then(result => {return result})
					console.log (userDetails);*/

					return true
				}
			else
				{	return false}
		}

		else
		{
			return false
		}
	};
