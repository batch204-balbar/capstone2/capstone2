const express = require("express");
const auth = require("../auth");
const productController = require("../controllers/productController");

const router = express.Router();

router.post("/create", auth.verify, (req, res) => 
	{
    const isAdmin = auth.decode(req.headers.authorization).isAdmin
    console.log(isAdmin);
     if (isAdmin) {
        productController.create(req.body).then(resultFromController => res.send(resultFromController));   
    } else {
        res.send(false);
    }
    
});
router.get("/all", (req, res) => {
    productController.getAllActive().then(resultFromController => res.send(resultFromController));

});

router.get("/:productId", auth.verify, (req, res) => {
	productController.retrieve(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	productController.update(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId/archive", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization);
	productController.archive(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
});

router.post("/order" , auth.verify, (req, res) =>
    {
        let data =
        {
            userId: auth.decode(req.headers.authorization).id,
            isAdmin: auth.decode(req.headers.authorization).isAdmin
        }

        let productData =
        {
            products: req.body.products
        }

        userController.checkOut(data, productData).then(resultFromController => res.send(resultFromController));
    });



module.exports = router;