const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth");

const router = express.Router();

router.post("/register", (req, res) => {
	userController.register(req.body).then(resultFromController => res.send(resultFromController));	
});

router.post("/login", (req, res) => {
	userController.login(req.body).then(resultFromController => res.send(resultFromController));
});

router.get('/:userId/userDetails', auth.verify, (req, res) => {
	userController.details(req.body, req.params).then(resultFromController => res.send(resultFromController));
});

router.post("/order" , auth.verify, (req, res) =>
	{
		let data =
		{
			userId: auth.decode(req.headers.authorization).id,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		let productData =
		{
			products: req.body.products,
			totalAmount: req.body.totalAmount
		}

		userController.checkOut(data, productData).then(resultFromController => res.send(resultFromController));
	});




module.exports = router;